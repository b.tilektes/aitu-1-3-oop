package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
//import kz.aitu.oop.repository.StudentDBRepository;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@RestController
@RequestMapping("/api/task/2")
@AllArgsConstructor
public class AssignmentController2 {

    //@Autowired
    //private StudentDBRepository studentDBRepository;

    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        Hashtable<String, List<Student>> group = new Hashtable<>();

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for(Student student: studentFileRepository.getStudents()) {
            if(!group.containsKey(student.getGroup())) {
                group.put(student.getGroup(), new ArrayList<>());
            }

            group.get(student.getGroup()).add(student);
        }

        double totalAverageGroupCount = 0;
        for (List<Student> studentList: group.values()) {
            totalAverageGroupCount += studentList.size();
        }

        return ResponseEntity.ok(totalAverageGroupCount / group.size());
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        double average = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double totalPoint = 0;
        int countStudent = 0;

        for (Student student: studentFileRepository.getStudents()) {
            totalPoint += student.getPoint();
            countStudent++;
        }

        average = totalPoint /countStudent;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;
        //change your code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double totalAge = 0;
        int countStudent = 0;

        for (Student student: studentFileRepository.getStudents()) {
            totalAge += student.getAge();
            countStudent++;
        }

        average = totalAge /countStudent;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double highestStudentsPoint = 0;

        for (Student student: studentFileRepository.getStudents()) {
            if(student.getPoint() > highestStudentsPoint) highestStudentsPoint = student.getPoint();
        }

        return ResponseEntity.ok(highestStudentsPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double highestStudentsAge = 0;

        for (Student student: studentFileRepository.getStudents()) {
            if(student.getPoint() > highestStudentsAge) highestStudentsAge = student.getAge();
        }

        return ResponseEntity.ok(highestStudentsAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        Hashtable<String, List<Student>> group = new Hashtable<>();

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for(Student student: studentFileRepository.getStudents()) {
            if(!group.containsKey(student.getGroup())) {
                group.put(student.getGroup(), new ArrayList<>());
            }

            group.get(student.getGroup()).add(student);
        }

        double highestGroupAveragePoint = 0;

        String result = "";
        for (List<Student> studentList: group.values()) {
            double totalGroupPoint = 0;

            for (Student student: studentList) {
                totalGroupPoint += student.getPoint();
            }
            double averageGroupPoint = totalGroupPoint / studentList.size();

            //result += studentList.get(0).getGroup() + ": " + averageGroupPoint + "</br>";

            if(averageGroupPoint > highestGroupAveragePoint) highestGroupAveragePoint = averageGroupPoint;
        }

        return ResponseEntity.ok(highestGroupAveragePoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        Hashtable<String, List<Student>> group = new Hashtable<>();

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for(Student student: studentFileRepository.getStudents()) {
            if(!group.containsKey(student.getGroup())) {
                group.put(student.getGroup(), new ArrayList<>());
            }

            group.get(student.getGroup()).add(student);
        }

        double highestGroupAverageAge = 0;
        for (List<Student> studentList: group.values()) {
            double totalGroupAge = 0;

            for (Student student: studentList) {
                totalGroupAge += student.getAge();
            }
            double averageGroupAge = totalGroupAge / studentList.size();

            //System.out.println(studentList.get(0).getGroup() + ": " + averageGroupAge);

            if(averageGroupAge > highestGroupAverageAge) highestGroupAverageAge = averageGroupAge;
        }

        return ResponseEntity.ok(highestGroupAverageAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
