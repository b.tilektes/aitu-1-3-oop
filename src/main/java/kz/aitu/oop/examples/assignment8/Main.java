package kz.aitu.oop.examples.assignment8;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;


public class Main {
    private static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws RuntimeException {
        try {
            throw new Exception("in ExceptionTest.main()");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("In finally");
        }

        Object object = null;
        try {

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        int[] ab = new int[0];
        try {
            System.out.println(ab[1]);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new RuntimeException(e);
        }

        throw new FirstException();


    }

    private static void logException(Exception e) {
        StringWriter trace = new StringWriter();
        e.printStackTrace(new PrintWriter(trace));
        logger.severe(trace.toString());
    }
}