package kz.aitu.oop.examples.assignment8;

class SecondException extends RuntimeException {
    private int x;

    public SecondException(int x) {
        this.x = x;
    }

    public SecondException(String message, int x) {
        super(message);
        this.x = x;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + " x = " + x;
    }
}