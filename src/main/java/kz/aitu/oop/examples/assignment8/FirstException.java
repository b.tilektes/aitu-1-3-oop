package kz.aitu.oop.examples.assignment8;

class FirstException extends RuntimeException {
    private String message;

    public FirstException() {
    }

    public FirstException(String message) {
        super(message);
        this.message = message;
    }

    public void display() {
        System.out.println(message);
    }
}