package kz.aitu.oop.examples.jdbc;

public class Main {

    private Catalog catalog;
    private BorrowerDatabase borrowerDB;

    public Main(Catalog catalog, BorrowerDatabase borrowerDB) {
        this.catalog = catalog;
        this.borrowerDB = borrowerDB;
    }

    public void displayCatalog() {
        for (CatalogItem catalogItem : catalog.getItems()) {
            System.out.println(catalogItem);
        }
    }
}
