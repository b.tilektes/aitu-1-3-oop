package kz.aitu.oop.examples.jdbc;



import lombok.Data;

@Data
public class CatalogItem {

    private String code;
    private String title;
}
