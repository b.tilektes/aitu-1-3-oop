package kz.aitu.oop.examples.jdbc;

import java.util.ArrayList;
import java.util.List;

public class BorrowerDatabase {
    private List<Borrower> borrowerList;

    public BorrowerDatabase() {
        this.borrowerList = new ArrayList<>();
    }

    public void addBorrower(Borrower borrower) {
        borrowerList.add(borrower);
    }

    public Borrower getBorrower(String id) {
        for (Borrower borrower : borrowerList) {
            if(borrower.getId().equals(id)) return borrower;
        }

        return null;
    }
}

