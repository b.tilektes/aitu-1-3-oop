package kz.aitu.oop.examples.jdbc;


import lombok.Data;

        import java.util.ArrayList;
        import java.util.List;

public class Borrower {
    private String id;
    private String name;
    private List<CatalogItem> catalogItems;

    public Borrower() {
        catalogItems = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addItem(CatalogItem catalogItem) {
        catalogItems.add(catalogItem);
    }

    public CatalogItem getItem(String code) {
        for (CatalogItem catalogItem : catalogItems) {
            if(catalogItem.getCode().equals(code))
                return catalogItem;
        }
        return null;
    }
}
