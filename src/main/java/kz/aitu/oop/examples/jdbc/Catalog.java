package kz.aitu.oop.examples.jdbc;

import java.util.ArrayList;
import java.util.List;

public class Catalog {

    private List<CatalogItem> catalogItemList;

    public Catalog() {
        this.catalogItemList = new ArrayList<>();
    }

    public void addCatalogItem(CatalogItem catalogItem) {
        catalogItemList.add(catalogItem);
    }

    public CatalogItem getItem(String code) {
        for (CatalogItem catalogItem : catalogItemList) {
            if(catalogItem.getCode().equals(code)) return catalogItem;
        }

        return null;
    }

    public CatalogItem getItem(int index) {
        return catalogItemList.get(index);
    }

    public int getNumberOfItems() {
        return catalogItemList.size();
    }

    public List<CatalogItem> getItems() {
        return catalogItemList;
    }
}

