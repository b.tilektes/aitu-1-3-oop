package kz.aitu.oop.examples.Aqua;

public class FishWithAccess {
    public String name;
    public double cost;
    public double size;

    public FishWithAccess addFishName(String name){
        this.name = name;
        return this;
    }

    public FishWithAccess addFishCost(double cost){
        this.cost = cost;
        return this;
    }

    public FishWithAccess addFishSize(double size){
        this.size = size;
        return this;
    }

    public Fish make(){
        return new Fish(this);
    }
}
