package kz.aitu.oop.examples.Aqua;

public class RepWithAccess {

    public String name;
    public double cost;
    public double size;

    public RepWithAccess addReptileName(String name){
        this.name = name;
        return this;
    }

    public RepWithAccess addReptileCost(double cost){
        this.cost = cost;
        return this;
    }

    public RepWithAccess addReptileSize(double size){
        this.size = size;
        return this;
    }

    public Reptile make(){
        return new Reptile(this);
    }
}
