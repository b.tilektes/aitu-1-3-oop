package kz.aitu.oop.examples.Aqua;

public class Fish extends Aquarium{

    private String type;
    private String name;
    private double cost;
    private double size;

    public Fish(FishWithAccess fishwithaccess){
        this.type = "Fish";
        this.name = fishwithaccess.name;
        this.cost = fishwithaccess.cost;
        this.size = fishwithaccess.size;
    }

    public Fish(String type, String name, double size, double cost) {
        this.type = type;
        this.name = name;
        this.size = size;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public double getSize() {
        return size;
    }

}

