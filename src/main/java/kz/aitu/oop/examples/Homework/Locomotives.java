package kz.aitu.oop.examples.Homework;

public class Locomotives {
    public int id;
    public int mass;
    public Locomotives(int id, int mass) {
        this.id = id;
        this.mass = mass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }
}
