package kz.aitu.oop.examples.Homework;

public class RC {
    public int id;
    public SERC numPlaces;

    public RC(int id, SERC numPlaces, SERC availability, SERC mass) {
        this.id = id;
        this.numPlaces = numPlaces;
        this.availability = availability;
        this.mass = mass;
    }

    public SERC availability;
    public SERC mass;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SERC getNumPlaces() {
        return numPlaces;
    }

    public void setNumPlaces(SERC numPlaces) {
        this.numPlaces = numPlaces;
    }

    public SERC getAvailability() {
        return availability;
    }

    public void setAvailability(SERC availability) {
        this.availability = availability;
    }

    public SERC getMass() {
        return mass;
    }

    public void setMass(SERC mass) {
        this.mass = mass;
    }
}
