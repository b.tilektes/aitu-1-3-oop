package kz.aitu.oop.examples.Homework;

import lombok.Data;

@Data
public class ERC {
    public int id;
    public int numPlaces;
    public boolean availability;
    public int mass;


    public ERC(int id, int numPlaces, boolean availability, int mass) {
        this.id = id;
        this.numPlaces = numPlaces;
        this.availability = availability;
        this.mass = mass;
    }

}
