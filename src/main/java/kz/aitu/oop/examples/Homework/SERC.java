package kz.aitu.oop.examples.Homework;

import lombok.Data;

@Data
public class SERC {
public  ERC numPlaces;
public ERC mass;
public ERC availability;

    public SERC(ERC numPlaces, ERC mass, ERC availability) {
        this.numPlaces = numPlaces;
        this.mass = mass;
        this.availability = availability;
    }
}
