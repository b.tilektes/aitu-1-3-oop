package kz.aitu.oop.examples.Homework;

public class Train {
    public Locomotives mass;
    public RC numPlaces;
    public RC availability;

    public Train(Locomotives mass, RC numPlaces, RC availability) {
        this.mass = mass;
        this.numPlaces = numPlaces;
        this.availability = availability;
    }

    public Locomotives getMass() {
        return mass;
    }

    public void setMass(Locomotives mass) {
        this.mass = mass;
    }

    public RC getNumPlaces() {
        return numPlaces;
    }

    public void setNumPlaces(RC numPlaces) {
        this.numPlaces = numPlaces;
    }

    public RC getAvailability() {
        return availability;
    }

    public void setAvailability(RC availability) {
        this.availability = availability;
    }
}
