package kz.aitu.oop.examples.last;

public class SemiPrecious {
    private String type = "SemiPrecious";

    public SemiPrecious(String name, int cost) {
        super();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getCost() {
        return super.cost();
    }

    @Override
    public void setCost(int cost) {
        super.setCost(cost);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }
}
