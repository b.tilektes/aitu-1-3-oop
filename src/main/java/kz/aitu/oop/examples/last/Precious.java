package kz.aitu.oop.examples.last;

public class Precious {
    private String type = "Precious";

    public Precious(String name, int cost) {
        super();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public void setCost(int cost) {
        super.setCost(cost);
    }

    @Override
    public int getCost() {
        return super.getCost();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}
