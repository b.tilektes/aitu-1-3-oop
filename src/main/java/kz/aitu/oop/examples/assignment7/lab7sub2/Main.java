package kz.aitu.oop.examples.assignment7.lab7sub2;

public class Main {

    public static void main(String[] args){
        Movable m1 = new MovablePoint(10, 20, 65, 45);
        System.out.println(m1);
        m1.MoveLeft();
        System.out.println(m1);

        Movable m2 = new MovableCircle(4, 9, 4, 8, 20);
        System.out.println(m2);
        m2.MoveRight();
        System.out.println(m2);

    }

}
