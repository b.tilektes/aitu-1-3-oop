package kz.aitu.oop.examples.assignment7.sub3;

public interface GeometricObject {
    double getPerimeter();
    double getArea();

}
