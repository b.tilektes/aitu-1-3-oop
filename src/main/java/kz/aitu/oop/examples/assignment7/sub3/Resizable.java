package kz.aitu.oop.examples.assignment7.sub3;

public interface Resizable {
    double  resize(int percent);
}
