package kz.aitu.oop.examples.assignment7.sub1;

public class Main {

    public static void main(String[] args) {



        Circle s1 = new Circle("red", false, 5.5);  // Upcast Circle to Shape
        System.out.println(s1);                    // which version?
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());     // which version?
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
        System.out.println(s1.getRadius());

        System.out.println(s1);
        System.out.println(s1.getArea());
        System.out.println(s1.getArea());
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
        System.out.println(s1.getRadius());


        Shape s6 = new Shape();
        Rectangle s3 = new Rectangle("red", false, 1, 2); // Upcast
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());
        System.out.println(s3.getLength());

        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getColor());
        System.out.println(s3.getLength());


        Square s4 = new Square(6.6); // Upcast
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        System.out.println(s4.getSide());


        // Take note that we downcast Shape s4 to Rectangle,
// which is a superclass of Square, instead of Square
        System.out.println(s4);
        System.out.println(((Rectangle) s4).getArea());
        System.out.println(s4.getColor());
        System.out.println(s4.getSide());
        System.out.println(s4.getLength());


        // Downcast Rectangle r2 to Square
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        System.out.println(s4.getSide());
        System.out.println(s4.getLength());

    }
}

