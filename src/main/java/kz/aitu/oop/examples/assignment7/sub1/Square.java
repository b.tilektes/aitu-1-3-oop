package kz.aitu.oop.examples.assignment7.sub1;

public class Square extends Rectangle {

    public Square(double side) {
        super(side, side);
        this.width = side;
        this.length = side;
    }

    public double getSide() {
        return 0;
    }

    public Square(String color, boolean filled, double width, double length) {
        super(color, filled, width, length);
    }

    public Square(double weidth, double length) {
        super(weidth, length);
    }


    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public double getArea() {
        return super.getArea();
    }

    @Override
    public String toString() {
        return "Square{" +
                "length=" + length +
                ", width=" + width +
                '}' + " which is a subclass of " + super.toString();
    }

    public double setPerimetr(){
        return 0;
    }

}
