package kz.aitu.oop.examples.assignment7.lab7sub2;

public interface Movable {
    void MoveUp();
    void MoveDown();
    void MoveLeft();
    void MoveRight();
}
