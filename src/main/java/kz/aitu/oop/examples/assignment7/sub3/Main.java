package kz.aitu.oop.examples.assignment7.sub3;

public class Main {
    public static void main(String[] args) {
    Circle TestCircle = new Circle(2);
    System.out.println(TestCircle.getArea());

    Circle TestCircle2 = new Circle(3);
    System.out.println(TestCircle2.getPerimeter());

    ResizableCircle TestResizableCircle= new ResizableCircle(5);
    System.out.println(TestResizableCircle.resize(5));
    }
}
