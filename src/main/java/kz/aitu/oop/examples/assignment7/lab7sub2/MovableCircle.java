package kz.aitu.oop.examples.assignment7.lab7sub2;

public class MovableCircle implements Movable {


    private MovablePoint center;
    private int radius;

    public MovableCircle(int x,int y,int xSpeed,int ySpeed,int radius) {
        center=new MovablePoint(x,y,xSpeed,ySpeed);
    }

    @Override
    public void MoveUp() {
        center.y-=center.ySpeed;
    }

    @Override
    public void MoveDown() {
        center.y-=center.ySpeed;
    }

    @Override
    public void MoveLeft() {
        center.x-=center.xSpeed;
    }

    @Override
    public void MoveRight() {
        center.x-=center.xSpeed;

    }
}
