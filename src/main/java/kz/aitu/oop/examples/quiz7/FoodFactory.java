package kz.aitu.oop.examples.quiz7;

public class FoodFactory {
    public Food getFood(String order) {
        if (order.equalsIgnoreCase("cake")) {
            Food c = new Cake();
            return c;
        }
        else {
            Food p = (Food) new Pizza();
            return p;
        }

    }
}
