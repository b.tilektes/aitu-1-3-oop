package kz.aitu.oop.examples.quiz6;

        import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();
        Singletone singletone=Singletone.getSingleInstance();
        singletone.str=str;

        System.out.println("hello i am singleton.Let me say"
                +singletone.str +"to you");
    }
}
