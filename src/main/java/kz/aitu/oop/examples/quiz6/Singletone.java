package kz.aitu.oop.examples.quiz6;

public class Singletone{
    public String str="hello world";
    private static Singletone singleton = new Singletone();

    public Singletone() { }

    public static Singletone getSingleInstance() {
        return singleton;
    }
}
