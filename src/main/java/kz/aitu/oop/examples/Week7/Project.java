package kz.aitu.oop.examples.Week7;

public class Project {
    public String project_name;
    public double project_cost;

    public Project addProjectName(String project_name){
        this.project_name = project_name;
        return this;
    }

    public Project addProjectCost(double cost){
        this.project_cost = cost;
        return this;
    }
}
