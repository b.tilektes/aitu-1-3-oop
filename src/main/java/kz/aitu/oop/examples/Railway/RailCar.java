package kz.aitu.oop.examples.Railway;

public class RailCar extends Train {

    private String type;
    private String name;
    private double cost;


    public RailCar(Locomotives locomotives){
        this.type = "RailCar";
        this.name = locomotives.name;
    }

    public RailCar(String type, String name, double size, double cost) {
        this.type = type;
        this.name = name;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }


}

